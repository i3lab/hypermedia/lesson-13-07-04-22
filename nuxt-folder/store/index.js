
export const state = () => {
    return {
        counter: 0
    }
}

export const mutations = {
    increase(state) {
        state.counter += 1
    }

}

export const actions = {
    async anAction() {
        // Some async action
    }
}
